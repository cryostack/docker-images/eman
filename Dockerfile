FROM conda/miniconda2

RUN apt-get update && apt-get install libgl-dev git build-essential libglu1-mesa-dev freeglut3-dev mesa-common-dev -y

SHELL ["/bin/bash", "-c"]

RUN useradd -ms /bin/bash cryostack
USER cryostack

COPY src/eman2.3.linux64.sh .
RUN /bin/bash eman2.3.linux64.sh

ENV QT_X11_NO_MITSHM 1

ENV PATH=/home/cryostack/EMAN2/bin:/home/cryostack/EMAN2/bin:/home/cryostack/EMAN2/condabin:/opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

WORKDIR /home/cryostack/
